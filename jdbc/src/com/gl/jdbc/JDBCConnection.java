package com.gl.jdbc;

import java.sql.*;

public class JDBCConnection {

	private static String jdbcDriver = "com.mysql.jdbc.Driver";
	private static String url = "jdbc:mysql://localhost:3306/shylendra";
	private static String userName = "root";
	private static String password = "root";

	private static Connection connection;
	private static Statement statement;

	public static void main(String[] args) throws SQLException {
		connection = null;
		statement = null;

		try {
			// load JDBC driver
			Class.forName(jdbcDriver);
			System.out.println("connecting to selected database...");

			// get connection
			connection = DriverManager.getConnection(url, userName, password);
			System.out.println("Connected successfully");

			// create statement
			statement = connection.createStatement();

			// create data base if not exists
			String createDB = "CREATE DATABASE IF NOT EXISTS shylendra";
			if (createDB != null) {
				statement.executeUpdate(createDB);
				System.out.println("DB created successfully");
			}

			/*
			 * // Drop Data Base String sql = "DROP DATABASE shylendra";
			 * statement.executeUpdate(sql);
			 * System.out.println("Database deleted successfully...");
			 */

			// create table if not exists
			String table = "CREATE TABLE IF NOT EXISTS shylu (id INTEGER, name VARCHAR(30))";
			if (table != null) {
				statement.executeUpdate(table);
				System.out.println("Table created successfully");
			}

			/*
			 * // Drop table String sql = "DROP TABLE shylu ";
			 * statement.executeUpdate(sql);
			 * System.out.println("Table  deleted in given database...");
			 */

			// insert records
			String query1 = "INSERT INTO shylu VALUES(555, 'shylu')";
			String query2 = "insert into shylu VALUES(560, 'balu')";
			statement.executeUpdate(query1);
			statement.executeUpdate(query2);

			System.out.println("values inserted successfully");

			/*// insert records
			String query1 = "INSERT INTO student VALUES(1, 'shylu')";

			// execute query
			statement.executeUpdate(query1);
			String query2 = "insert into student " + "VALUES(2, 'balu')";
			statement.executeUpdate(query2);
			String query3 = "INSERT INTO student " + "VALUES(3, 'santu')";
			statement.executeUpdate(query3);
			String query4 = "INSERT INTO student " + "VALUES(4, 'raju')";
			statement.executeUpdate(query4);

			System.out.println("values inserted successfully");*/

			// retrieve records without any condition.
			System.out.println("Fetching records without condition...");
			String sql = "SELECT id, name FROM shylu";
			ResultSet rs = statement.executeQuery(sql);

			while (rs.next()) {
				// Retrieve by column name
				int id = rs.getInt("id");
				String name = rs.getString("name");

				// Display values
				System.out.print("ID: " + id);
				System.out.println(", Name: " + name);
			}

			// close resources
			connection.close();
			statement.close();

			System.out.println("connection closed successfully");

		} catch (SQLException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (connection != null) {
				connection.close();
			}
			if (statement != null) {
				statement.close();
				// System.out.println("connection closed successfully");
			}
		}
		System.out.println("Good bye");
	}
}
