package com.gl.jdbc;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class TestJDBC {

	private static String driverName = "com.mysql.jdbc.Driver";
	private static String url = "jdbc:mysql://localhost:3306/";
	private static String userName = "root";
	private static String password = "root";
	static Statement statement = null;
	static Connection connection = null;
	private static String dbName = "jdtest";

	public static void main(String[] args) throws SQLException {

		// load driver
		try {
			try {
				Class.forName(driverName);
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			}

			// get connection
			connection = DriverManager.getConnection(url, userName, password);
			System.out.println("connection got success");

			// create statement
			statement = connection.createStatement();

			// execute query
			// create DB if not exists
			String db = "CREATE DATABASE IF NOT EXISTS " + dbName;
			statement.executeUpdate(db);
			System.out.println("DB created");

			// connect to created Data base
			// get new connection
			url = "jdbc:mysql://localhost:3306/" + dbName;
			connection = DriverManager.getConnection(url, userName, password);
			System.out.println("connection got success");

			// create statement
			statement = connection.createStatement();

			// create table if not exists
			String table = "CREATE TABLE IF NOT EXISTS test (id INTEGER, Name VARCHAR(50))";
			statement.executeUpdate(table);
			System.out.println("table created");

			// insert values
			String insert = "INSERT INTO test VALUES(5, 'shylu')";
			statement.executeUpdate(insert);
			System.out.println("values inserted");

			// retrieving values
			String retrieve = "SELECT * FROM test ";
			ResultSet rs = statement.executeQuery(retrieve);
			while (rs.next()) {
				int id = rs.getInt("id");
				String name = rs.getString("name");
				// display
				System.out.println("Id is: " + id);
				System.out.println("Name is: " + name);
			}

			// close connections
			statement.close();
			connection.close();

			System.out.println("connection closed");

		} catch (SQLException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			statement.close();
			connection.close();
		}

	}

}
